# Mass Spring Model Wang et al
This repository contains the mass-spring cell growth model of Yang et al.

The model is implemented in VLab (http://algorithmicbotany.org/vlab/) and runs in MorphoDynamX, the modelling platform of MorphoGraphX (http://morphodynamx.org; http://morphographx.org).

An installation package for MorphoDynamX (for Mint 20.2 and Cuda 11) is in this repository.

To run the model, first install MorphoDynamX and VLab, then open a console in the folder "02-MassSpringWallAge" and type: "make" followed by "make run".

For a detailed model description please refer to the paper and the Model_despription.jpg file in this repository. 
